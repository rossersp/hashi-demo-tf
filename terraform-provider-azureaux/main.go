package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	azureaux "rosshinkley/terraform-provider-azureaux/azureaux"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: azureaux.Provider,
	})
}
