package azureaux

import (
	"fmt"
	"time"

	"github.com/Azure/go-autorest/autorest/azure"
	"github.com/hashicorp/go-azure-helpers/authentication"
	"github.com/hashicorp/go-azure-helpers/sender"
	"rosshinkley/terraform-provider-azureaux/azureaux/internal/clients"
	"rosshinkley/terraform-provider-azureaux/azureaux/internal/common"
	"rosshinkley/terraform-provider-azureaux/azureaux/internal/services/resource"
	"rosshinkley/terraform-provider-azureaux/azureaux/internal/services/storage"
	"rosshinkley/terraform-provider-azureaux/azureaux/internal/services/subscription"
)

// ArmClient contains the handles to all the specific Azure Resource Manager
// resource classes' respective clients.
type ArmClient struct {
	// inherit the fields from the parent, so that we should be able to set/access these at either level
	clients.Client

	clientId                    string
	tenantId                    string
	subscriptionId              string
	partnerId                   string
	usingServicePrincipal       bool
	environment                 azure.Environment
	skipProviderRegistration    bool
	disableCorrelationRequestID bool

	AuthConfig *authentication.Config

	// Services
	// NOTE: all new services should be Public as they're going to be relocated in the near-future
	Resource     *resource.Client
	Storage      *storage.Client
	Subscription *subscription.Client
}

// getArmClient is a helper method which returns a fully instantiated
// *ArmClient based on the Config's current settings.
func getArmClient(authConfig *authentication.Config, skipProviderRegistration bool, partnerId string, disableCorrelationRequestID bool) (*ArmClient, error) {
	trace("getting new arm client")
	env, err := authentication.DetermineEnvironment(authConfig.Environment)
	if err != nil {
		return nil, err
	}

	// client declarations:
	trace("setting up client")
	client := ArmClient{
		Client: clients.Client{},

		AuthConfig: authConfig,

		clientId:                    authConfig.ClientID,
		tenantId:                    authConfig.TenantID,
		subscriptionId:              authConfig.SubscriptionID,
		partnerId:                   partnerId,
		environment:                 *env,
		usingServicePrincipal:       authConfig.AuthenticatedAsAServicePrincipal,
		skipProviderRegistration:    skipProviderRegistration,
		disableCorrelationRequestID: disableCorrelationRequestID,
	}

	trace("setting up oauth")
	oauthConfig, err := authConfig.BuildOAuthConfig(env.ActiveDirectoryEndpoint)
	if err != nil {
		return nil, err
	}

	// OAuthConfigForTenant returns a pointer, which can be nil.
	if oauthConfig == nil {
		return nil, fmt.Errorf("Unable to configure OAuthConfig for tenant %s", authConfig.TenantID)
	}

	trace("building sender")
	sender := sender.BuildSender("AzureRM")

	// Resource Manager endpoints
	trace("setting up resource endpoints")
	endpoint := env.ResourceManagerEndpoint
	trace("getting resource authorization")
	auth, err := authConfig.GetAuthorizationToken(sender, oauthConfig, env.TokenAudience)
	if err != nil {
		return nil, err
	}

	// Graph Endpoints
	trace("setting up graph endpoints")
	graphEndpoint := env.GraphEndpoint
	trace("setting up graph authorization")
	graphAuth, err := authConfig.GetAuthorizationToken(sender, oauthConfig, graphEndpoint)
	if err != nil {
		return nil, err
	}

	// Key Vault Endpoints
	trace("setting up keyvault endpoints")
	keyVaultAuth := authConfig.BearerAuthorizerCallback(sender, oauthConfig)

	trace("setting up client options")
	o := &common.ClientOptions{
		SubscriptionId:              authConfig.SubscriptionID,
		TenantID:                    authConfig.TenantID,
		PartnerId:                   partnerId,
		GraphAuthorizer:             graphAuth,
		GraphEndpoint:               graphEndpoint,
		KeyVaultAuthorizer:          keyVaultAuth,
		ResourceManagerAuthorizer:   auth,
		ResourceManagerEndpoint:     endpoint,
		PollingDuration:             180 * time.Minute,
		SkipProviderReg:             skipProviderRegistration,
		DisableCorrelationRequestID: disableCorrelationRequestID,
		Environment:                 *env,
	}

	trace("building resource client")
	client.Resource = resource.BuildClient(o)
	trace("building storage client")
	client.Storage = storage.BuildClient(o)
	trace("building subscription client")
	client.Subscription = subscription.BuildClient(o)

	return &client, nil
}
