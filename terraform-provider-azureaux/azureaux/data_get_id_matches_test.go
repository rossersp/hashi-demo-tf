package azureaux

import (
	"fmt"
	"os"
	"strconv"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"github.com/terraform-providers/terraform-provider-azurerm/azurerm/helpers/tf"
)

func TestAccAzureAuxGetIdMatches_subAndResourceGroupAndSubRegex(t *testing.T) {
	t.Skip()
}

func TestAccAzureAuxGetIdMatches_specificSubAndResourceGroup(t *testing.T) {
	resourceName := "data.azureaux_get_id_matches.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccAzureAuxGetIdMatches_rgAndSubID(ri, location, subscriptionID),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr(resourceName, "found_ids.#", "1"),
					resource.TestCheckResourceAttr(resourceName, "found_ids.0", fmt.Sprintf("/subscriptions/%s/resourceGroups/acctestRG-%d/providers/Microsoft.Storage/storageAccounts/mca%d", subscriptionID, ri, ri)),
				),
				ExpectNonEmptyPlan: true,
			},
		},
	})

}

func TestAccAzureAuxGetIdMatches_subAndSubRegex(t *testing.T) {
	t.Skip()
}

func TestAccAzureAuxGetIdMatches_subOnly(t *testing.T) {
	resourceName := "data.azureaux_get_id_matches.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccAzureAuxGetIdMatches_subID(ri, location, subscriptionID),
				Check: resource.ComposeTestCheckFunc(
					checkFoundIDsFor(resourceName, fmt.Sprintf("/subscriptions/%s/resourceGroups/acctestRG-%d/providers/Microsoft.Storage/storageAccounts/mca%d", subscriptionID, ri, ri)),
				),
				ExpectNonEmptyPlan: true,
			},
		},
	})
}

func TestAccAzureAuxGetIdMatches_resourceGroupAndSubRegex(t *testing.T) {
	resourceName := "data.azureaux_get_id_matches.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccAzureAuxGetIdMatches_rgAndSubRegex(ri, location, ".*Atlas.*Sand.*"),
				Check: resource.ComposeTestCheckFunc(
					checkFoundIDsFor(resourceName, fmt.Sprintf("/subscriptions/%s/resourceGroups/acctestRG-%d/providers/Microsoft.Storage/storageAccounts/mca%d", subscriptionID, ri, ri)),
				),
				ExpectNonEmptyPlan: true,
			},
		},
	})
}

func TestAccAzureAuxGetIdMatches_resourceGroup(t *testing.T) {
	resourceName := "data.azureaux_get_id_matches.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccAzureAuxGetIdMatches_rgOnly(ri, location),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr(resourceName, "found_ids.#", "1"),
					resource.TestCheckResourceAttr(resourceName, "found_ids.0", fmt.Sprintf("/subscriptions/%s/resourceGroups/acctestRG-%d/providers/Microsoft.Storage/storageAccounts/mca%d", subscriptionID, ri, ri)),
				),
				ExpectNonEmptyPlan: true,
			},
		},
	})
}

func TestAccAzureAuxGetIdMatches_subRegex(t *testing.T) {
	resourceName := "data.azureaux_get_id_matches.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccAzureAuxGetIdMatches_subRegex(ri, location, ".*Atlas.*Sand.*"),
				Check: resource.ComposeTestCheckFunc(
					checkFoundIDsFor(resourceName, fmt.Sprintf("/subscriptions/%s/resourceGroups/acctestRG-%d/providers/Microsoft.Storage/storageAccounts/mca%d", subscriptionID, ri, ri)),
				),
				ExpectNonEmptyPlan: true,
			},
		},
	})

}

func TestAccAzureAuxGetIdMatches_nothingSpecified(t *testing.T) {
	resourceName := "data.azureaux_get_id_matches.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccAzureAuxGetIdMatches_nothingSpecified(ri, location),
				Check: resource.ComposeTestCheckFunc(
					checkFoundIDsFor(resourceName, fmt.Sprintf("/subscriptions/%s/resourceGroups/acctestRG-%d/providers/Microsoft.Storage/storageAccounts/mca%d", subscriptionID, ri, ri)),
				),
				ExpectNonEmptyPlan: true,
			},
		},
	})
}

func testAccAzureAuxGetIdMatches_template(rInt int, location string) string {
	return fmt.Sprintf(`
resource "azurerm_resource_group" "test" {
	name     = "acctestRG-%d"
	location = "%s"
}

resource "azurerm_storage_account" "test" {
	name                     = "mca%d"
  resource_group_name      = "${azurerm_resource_group.test.name}"
  location                 = "${azurerm_resource_group.test.location}"
	account_tier             = "Standard"
  account_replication_type = "LRS"
}

`, rInt, location, rInt)
}

func testAccAzureAuxGetIdMatches_subID(rInt int, location, subID string) string {
	template := testAccAzureAuxGetIdMatches_template(rInt, location)
	return fmt.Sprintf(`
%s

data "azureaux_get_id_matches" "test" {
	subscription_id = "%s"
	find_regex      = ".*mca[0-9]+"
	depends_on = [azurerm_resource_group.test, azurerm_storage_account.test]
}
`, template, subID)
}

func testAccAzureAuxGetIdMatches_subRegex(rInt int, location, subRegex string) string {
	template := testAccAzureAuxGetIdMatches_template(rInt, location)
	return fmt.Sprintf(`
%s

data "azureaux_get_id_matches" "test" {
	subscription_regex = "%s"
	find_regex         = ".*mca[0-9]+"
	depends_on = [azurerm_resource_group.test, azurerm_storage_account.test]
}
`, template, subRegex)
}

func testAccAzureAuxGetIdMatches_rgOnly(rInt int, location string) string {
	template := testAccAzureAuxGetIdMatches_template(rInt, location)
	return fmt.Sprintf(`
%s

data "azureaux_get_id_matches" "test" {
	resource_group_name = "acctestRG-%d"
	find_regex          = ".*mca[0-9]+"
	depends_on = [azurerm_resource_group.test, azurerm_storage_account.test]
}
`, template, rInt)

}

func testAccAzureAuxGetIdMatches_rgAndSubID(rInt int, location, subID string) string {
	template := testAccAzureAuxGetIdMatches_template(rInt, location)
	return fmt.Sprintf(`
%s

data "azureaux_get_id_matches" "test" {
	subscription_id     = "%s"
	resource_group_name = "acctestRG-%d"
	find_regex          = ".*mca[0-9]+"
	depends_on = [azurerm_resource_group.test, azurerm_storage_account.test]
}
`, template, subID, rInt)

}

func testAccAzureAuxGetIdMatches_rgAndSubRegex(rInt int, location, subRegex string) string {
	template := testAccAzureAuxGetIdMatches_template(rInt, location)
	return fmt.Sprintf(`
%s

data "azureaux_get_id_matches" "test" {
	subscription_regex  = "%s"
	resource_group_name = "acctestRG-%d"
	find_regex          = ".*mca[0-9]+"
	depends_on = [azurerm_resource_group.test, azurerm_storage_account.test]
}
`, template, subRegex, rInt)

}

func testAccAzureAuxGetIdMatches_nothingSpecified(rInt int, location string) string {
	template := testAccAzureAuxGetIdMatches_template(rInt, location)
	return fmt.Sprintf(`
%s

data "azureaux_get_id_matches" "test" {
	find_regex          = ".*mca[0-9]+"
	depends_on = [azurerm_resource_group.test, azurerm_storage_account.test]
}
`, template)

}

func checkFoundIDsFor(resourceName string, toCheck string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[resourceName]
		if !ok {
			return fmt.Errorf("Not found: %s", resourceName)
		}
		foundIdCount, err := strconv.Atoi(rs.Primary.Attributes["found_ids.#"])
		if err != nil {
			return err
		}
		for i := 0; i < foundIdCount; i++ {
			foundId := rs.Primary.Attributes[fmt.Sprintf("found_ids.%d", i)]
			if foundId == toCheck {
				return nil
			}
		}
		return fmt.Errorf("resource matching %q could not be found.", toCheck)
	}
}
