package storage

import (
	"github.com/Azure/azure-sdk-for-go/services/storage/mgmt/2017-06-01/storage"

	"rosshinkley/terraform-provider-azureaux/azureaux/internal/common"
)

type Client struct {
	StorageAccountsClient *storage.AccountsClient
}

func BuildClient(o *common.ClientOptions) *Client {
	StorageAccountsClient := storage.NewAccountsClientWithBaseURI(o.ResourceManagerEndpoint, o.SubscriptionId)
	o.ConfigureClient(&StorageAccountsClient.Client, o.ResourceManagerAuthorizer)

	return &Client{
		StorageAccountsClient: &StorageAccountsClient,
	}
}
