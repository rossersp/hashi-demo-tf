package azureaux

import (
	"log"
)

// traces log messages.  The `waitForResource` is tacked on to make searching
// for log messages in the cloud-init bootlog easier.  Takes the same arguments
// and works the same way as `log.Printf`.
func trace(format string, v ...interface{}) {
	log.Printf("azureaux "+format, v...)
}
