package azureaux

import (
	"bytes"
	"context"
	"fmt"
	"net/url"
	"regexp"
	"time"

	"github.com/Azure/azure-sdk-for-go/services/storage/mgmt/2017-06-01/storage"
	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func resourceWaitForBlobContains() *schema.Resource {
	return &schema.Resource{
		Create: resourceWaitForBlobContainsCreate,
		Read:   resourceWaitForBlobContainsRead,
		Update: resourceWaitForBlobContainsUpdate,
		Delete: resourceWaitForBlobContainsDelete,
		Schema: map[string]*schema.Schema{
			"resource_group_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"storage_account_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"container_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"blob_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"regex": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"timeout": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
				Default:  3600,
			},
			"state": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func getBlobContents(ctx context.Context, client *storage.AccountsClient, resourceGroup string, storageAccount string, container string, blob string) (string, error) {
	response, err := client.ListKeys(ctx, resourceGroup, storageAccount)
	if err != nil {
		return "", err
	}
	// for now, always use the primary key
	key := *(((*response.Keys)[0]).Value)

	// create a default request pipeline using your storage account name and account key
	credential, err := azblob.NewSharedKeyCredential(storageAccount, key)
	if err != nil {
		return "", err
	}
	p := azblob.NewPipeline(credential, azblob.PipelineOptions{})

	URL, err := url.Parse(
		fmt.Sprintf("https://%s.blob.core.windows.net/%s", storageAccount, container))
	if err != nil {
		return "", err
	}

	// Create a ContainerURL object that wraps the container URL and a request
	// pipeline to make requests.
	containerURL := azblob.NewContainerURL(*URL, p)
	blobURL := containerURL.NewBlockBlobURL(blob)
	downloadResponse, err := blobURL.Download(context.TODO(), 0, azblob.CountToEnd, azblob.BlobAccessConditions{}, false)
	if err != nil {
		return "", err
	}
	bodyStream := downloadResponse.Body(azblob.RetryReaderOptions{MaxRetryRequests: 20})

	// read the body into a buffer
	downloadedData := bytes.Buffer{}
	_, err = downloadedData.ReadFrom(bodyStream)
	if err != nil {
		return "", err
	}

	return downloadedData.String(), nil
}

func resourceWaitForBlobContainsCreate(d *schema.ResourceData, meta interface{}) error {
	trace("resourceWaitForBlobContainsCreate")

	// get the inputs and set the id
	resourceGroupName := d.Get("resource_group_name").(string)
	storageAccount := d.Get("storage_account_name").(string)
	container := d.Get("container_name").(string)
	blob := d.Get("blob_name").(string)
	findRegexStr := d.Get("regex").(string)
	d.SetId(fmt.Sprintf("waitforblobcontains_%s_%s_%s_%s_%s", resourceGroupName, storageAccount, container, blob, findRegexStr))

	accountsClient := meta.(*ArmClient).Storage.StorageAccountsClient
	ctx := meta.(*ArmClient).StopContext
	findRegex, err := regexp.Compile(findRegexStr)
	if err != nil {
		return err
	}

	foundContents := false
	for i := 0; i < d.Get("timeout").(int); i += 60 {
		time.Sleep(time.Duration(60) * time.Second)
		// grab the blob contents for the blob specified
		contents, err := getBlobContents(ctx, accountsClient, resourceGroupName, storageAccount, container, blob)
		if err != nil {
			return err
		}

		// does it contain what we're looking for?
		if findRegex.MatchString(contents) {
			// sweet, we're good to go
			foundContents = true
			break
		}
	}

	if foundContents {
		d.Set("state", "success")
		return nil
	} else {
		// timed out
		return fmt.Errorf("blob did not contain the contents specified in the time alotted")
	}
}

func resourceWaitForBlobContainsRead(d *schema.ResourceData, meta interface{}) error {
	return nil
}

func resourceWaitForBlobContainsUpdate(d *schema.ResourceData, meta interface{}) error {
	return resourceWaitForBlobContainsCreate(d, meta)
}

func resourceWaitForBlobContainsDelete(d *schema.ResourceData, meta interface{}) error {
	d.SetId("")
	return nil
}
