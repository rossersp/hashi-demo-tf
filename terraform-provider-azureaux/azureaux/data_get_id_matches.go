package azureaux

import (
	"context"
	"fmt"
	"os"
	"regexp"

	"github.com/Azure/azure-sdk-for-go/services/resources/mgmt/2018-05-01/resources"
	"github.com/Azure/azure-sdk-for-go/services/resources/mgmt/2018-06-01/subscriptions"
	"github.com/hashicorp/go-azure-helpers/authentication"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func dataGetIdMatches() *schema.Resource {
	return &schema.Resource{
		Read: dataGetIdMatchesRead,
		Schema: map[string]*schema.Schema{
			"resource_group_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: false,
				Optional: true,
			},
			"subscription_id": &schema.Schema{
				Type:     schema.TypeString,
				Required: false,
				Optional: true,
			},
			"subscription_regex": &schema.Schema{
				Type:     schema.TypeString,
				Required: false,
				Optional: true,
			},
			"find_regex": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"found_ids": &schema.Schema{
				Type:     schema.TypeList,
				Computed: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
		},
	}
}

// gets a resource listing by resource group
func getResourcesByResourceGroup(resourceGroupName string, ctx context.Context, resourcesClient *resources.Client) ([]resources.GenericResource, error) {
	resourcePage, err := resourcesClient.ListByResourceGroup(ctx, resourceGroupName, "", "", nil)
	if err != nil {
		return nil, err
	}
	resourceList := resourcePage.Values()
	for {
		if resourcePage.NotDone() {
			err = resourcePage.Next()
			if err != nil {
				return nil, err
			}
			resourceList = append(resourceList, resourcePage.Values()...)
		} else {
			break
		}
	}
	return resourceList, nil
}

// gets a listing of the subscriptions
func getSubscriptions(ctx context.Context, subscriptionClient *subscriptions.Client) ([]subscriptions.Subscription, error) {
	resultsListPage, err := subscriptionClient.List(ctx)
	if err != nil {
		return nil, err
	}
	resultList := resultsListPage.Values()
	for {
		if resultsListPage.NotDone() {
			err = resultsListPage.Next()
			if err != nil {
				return nil, err
			}
			resultList = append(resultList, resultsListPage.Values()...)
		} else {
			break
		}
	}
	return resultList, nil
}

// produces a new arm client for the given subscription
func getUpdatedClient(subscriptionID string, ctx context.Context, baseClient *ArmClient) (*ArmClient, error) {
	clientSecret := os.Getenv("ARM_CLIENT_SECRET")
	builder := &authentication.Builder{
		SubscriptionID: subscriptionID,
		ClientID:       baseClient.AuthConfig.ClientID,
		ClientSecret:   clientSecret,
		TenantID:       baseClient.AuthConfig.TenantID,
		Environment:    baseClient.AuthConfig.Environment,
		MsiEndpoint:    "",

		// Feature Toggles
		SupportsClientCertAuth:         true,
		SupportsClientSecretAuth:       true,
		SupportsManagedServiceIdentity: false,
		SupportsAzureCliToken:          true,
		SupportsAuxiliaryTenants:       false,

		// Doc Links
		ClientSecretDocsLink: "https://www.terraform.io/docs/providers/azurerm/auth/service_principal_client_secret.html",
	}

	updatedAuthConfig, err := builder.Build()
	trace("getting updated client for %q", subscriptionID)
	trace("copied auth config for %q", subscriptionID)
	if err != nil {
		return nil, err
	}
	updatedAuthConfig.SubscriptionID = subscriptionID
	return getArmClient(updatedAuthConfig, baseClient.skipProviderRegistration, baseClient.partnerId, baseClient.disableCorrelationRequestID)
}

func getMatchingSubscriptions(subscriptionRegex string, ctx context.Context, subscriptionClient *subscriptions.Client) ([]subscriptions.Subscription, error) {
	allSubscriptions, err := getSubscriptions(ctx, subscriptionClient)
	if err != nil {
		return nil, err
	}

	findRegex, err := regexp.Compile(subscriptionRegex)
	if err != nil {
		return nil, err
	}

	var matchedSubs []subscriptions.Subscription
	for _, subscription := range allSubscriptions {
		trace("checking sub %q...", *subscription.DisplayName)
		if findRegex.MatchString(*subscription.DisplayName) {
			trace("sub %q matches", *subscription.DisplayName)
			matchedSubs = append(matchedSubs, subscription)
		}
	}
	return matchedSubs, nil
}

// gets a resource listing by a given subscription ID
func getResourcesBySubscription(subscriptionID string, ctx context.Context, baseClient *ArmClient) ([]resources.GenericResource, error) {
	trace("getting resources for %q", subscriptionID)
	newClient, err := getUpdatedClient(subscriptionID, ctx, baseClient)
	if err != nil {
		return nil, err
	}

	resourcePage, err := newClient.Resource.ResourcesClient.List(ctx, "", "", nil)
	if err != nil {
		return nil, err
	}
	resourceList := resourcePage.Values()
	for {
		if resourcePage.NotDone() {
			err = resourcePage.Next()
			if err != nil {
				return nil, err
			}
			resourceList = append(resourceList, resourcePage.Values()...)
		} else {
			break
		}
	}
	trace("got %d resources from %q", len(resourceList), subscriptionID)
	return resourceList, nil
}

// takes the standard resource data and metadata interface from the calling
// terraform instance.  Returns an error should one occur.
func dataGetIdMatchesRead(d *schema.ResourceData, meta interface{}) error {
	resourceGroupName, rgExists := d.GetOk("resource_group_name")
	subscriptionId, subExists := d.GetOk("subscription_id")
	subscriptionRegex, subRegexExists := d.GetOk("subscription_regex")
	findRegexStr := d.Get("find_regex").(string)
	d.SetId(fmt.Sprintf("waitforexists_%s_%s_%s_%s", resourceGroupName, subscriptionId, subscriptionRegex, findRegexStr))

	ctx := meta.(*ArmClient).StopContext
	trace("got clients")

	findRegex, err := regexp.Compile(findRegexStr)
	if err != nil {
		return err
	}

	var matches []string

	var resourceList []resources.GenericResource

	if subExists && rgExists && subRegexExists {
		// invalid combo
		return fmt.Errorf("specify either `subscription_id` or `subscription_regex`, not both.")
	} else if subExists && rgExists && !subRegexExists {
		trace("scanning specific sub and rg")
		// scan specific subscription + rg
		newClient, err := getUpdatedClient(subscriptionId.(string), ctx, meta.(*ArmClient))
		if err != nil {
			return err
		}
		resourceList, err = getResourcesByResourceGroup(resourceGroupName.(string), ctx, newClient.Resource.ResourcesClient)
		if err != nil {
			return err
		}
	} else if subExists && !rgExists && subRegexExists {
		// invalid combo
		return fmt.Errorf("specify either `subscription_id` or `subscription_regex`, not both.")
	} else if subExists && !rgExists && !subRegexExists {
		// scan a specific subscription
		resourceList, err = getResourcesBySubscription(subscriptionId.(string), ctx, meta.(*ArmClient))
		if err != nil {
			return err
		}
	} else if !subExists && rgExists && subRegexExists {
		// scan matching subscription friendly names for resources in the specified RG
		subs, err := getMatchingSubscriptions(subscriptionRegex.(string), ctx, meta.(*ArmClient).Subscription.Client)
		if err != nil {
			return err
		}
		for _, subscription := range subs {
			trace("checking %q for rg %q", *subscription.DisplayName, resourceGroupName.(string))
			newClient, err := getUpdatedClient(*subscription.SubscriptionID, ctx, meta.(*ArmClient))
			rs, err := getResourcesByResourceGroup(resourceGroupName.(string), ctx, newClient.Resource.ResourcesClient)
			if err != nil {
				return err
			}
			resourceList = append(resourceList, rs...)
		}
	} else if !subExists && rgExists && !subRegexExists {
		// scan an rg on the default subscription
		resourceList, err = getResourcesByResourceGroup(resourceGroupName.(string), ctx, meta.(*ArmClient).Resource.ResourcesClient)
		if err != nil {
			return err
		}
	} else if !subExists && !rgExists && subRegexExists {
		// scan matching subscription friendly names
		subs, err := getMatchingSubscriptions(subscriptionRegex.(string), ctx, meta.(*ArmClient).Subscription.Client)
		if err != nil {
			return err
		}
		for _, subscription := range subs {
			rs, err := getResourcesBySubscription(*subscription.SubscriptionID, ctx, meta.(*ArmClient))
			if err != nil {
				return err
			}
			resourceList = append(resourceList, rs...)
		}
	} else if !subExists && !rgExists && !subRegexExists {
		// scan all subscriptions
		subscriptions, err := getSubscriptions(ctx, meta.(*ArmClient).Subscription.Client)
		if err != nil {
			return err
		}
		for _, subscription := range subscriptions {
			rs, err := getResourcesBySubscription(*subscription.SubscriptionID, ctx, meta.(*ArmClient))
			if err != nil {
				return err
			}
			resourceList = append(resourceList, rs...)
		}
	}

	trace("found %d resources total.", len(resourceList))
	for _, resource := range resourceList {
		trace("checking %q...", *resource.ID)
		if findRegex.MatchString(*resource.ID) {
			trace("found id: %q", *resource.ID)
			matches = append(matches, *resource.ID)
		}
	}

	d.Set("found_ids", matches)
	return nil
}
