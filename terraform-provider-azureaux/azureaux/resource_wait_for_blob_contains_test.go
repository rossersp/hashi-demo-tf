package azureaux

import (
	"fmt"
	"os"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/terraform-providers/terraform-provider-azurerm/azurerm/helpers/tf"
)

func TestAzureAux_BlobContains(t *testing.T) {
	resourceName := "azureaux_wait_for_blob_contains.test"
	location := testLocation()
	ri := tf.AccRandTimeInt()
	subscriptionID := os.Getenv("ARM_SUBSCRIPTION_ID")

	resource.ParallelTest(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: makeABlob(ri, location, subscriptionID),
				Check: resource.ComposeTestCheckFunc(
					resource.TestCheckResourceAttr(resourceName, "state", "success"),
				),
			},
		},
	})

}

func makeABlob(ri int, location, subscriptionID string) string {
	return fmt.Sprintf(`
resource "azurerm_resource_group" "test" {
	name     = "acctestRG-%d"
	location = "%s"
}

resource "azurerm_storage_account" "test" {
	name                     = "sa%d"
  resource_group_name      = azurerm_resource_group.test.name
  location                 = azurerm_resource_group.test.location
	account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "test" {
	name = "sc%d"
	storage_account_name = azurerm_storage_account.test.name
	container_access_type = "private"
}

resource "azurerm_storage_blob" "test" {
	storage_account_name = azurerm_storage_account.test.name
	storage_container_name = azurerm_storage_container.test.name
	type = "block"
	name = "blob%d"
	source_content = "hello world"
}

resource "azureaux_wait_for_blob_contains" "test" {
	resource_group_name  = azurerm_resource_group.test.name
	storage_account_name = azurerm_storage_account.test.name
	container_name = azurerm_storage_container.test.name
	blob_name = azurerm_storage_blob.test.name
	regex = ".+world"
}

`, ri, location, ri, ri, ri)
}
