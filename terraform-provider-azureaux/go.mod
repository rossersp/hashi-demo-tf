module rosshinkley/terraform-provider-azureaux

replace git.apache.org/thrift.git => github.com/apache/thrift v0.0.0-20180902110319-2566ecd5d999

go 1.12

require (
	github.com/Azure/azure-sdk-for-go v34.1.0+incompatible
	github.com/Azure/azure-storage-blob-go v0.8.0
	github.com/Azure/go-autorest/autorest v0.9.1
	github.com/apache/thrift v0.12.0 // indirect
	github.com/gruntwork-io/terratest v0.23.3 // indirect
	github.com/hashicorp/go-azure-helpers v0.9.0
	github.com/hashicorp/go-uuid v1.0.1
	github.com/hashicorp/terraform v0.12.9
	github.com/hashicorp/terraform-plugin-sdk v1.3.0
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/openzipkin/zipkin-go v0.1.6 // indirect
	github.com/terraform-providers/terraform-provider-azuread v0.6.1-0.20191007035844-361c0a206ad4
	github.com/terraform-providers/terraform-provider-azurerm v1.36.1
	github.com/tombuildsstuff/giovanni v0.6.0
	golang.org/x/build v0.0.0-20190111050920-041ab4dc3f9d // indirect
)
