#! /bin/bash

loc=$(readlink -f ${BASH_SOURCE%/*})
parent=$(readlink -f $loc/..)

pushd $parent

terraform plan -out=./demo.out && terraform apply ./demo.out

popd
