#! /bin/bash

echo "hello from the setup script!" | tee -a output

# auth to the local azure services
response=$(curl 'http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https%3A%2F%2Fstorage.azure.com%2F' -H Metadata:true -s)
echo "got response: $response" | tee -a output
access_token=$(echo $response | python -c 'import sys, json; print (json.load(sys.stdin)["access_token"])')
echo "got token: $access_token" | tee -a output

# this is where we'd ordinarily do something interesting
# install applications, libraries, setup, etc
# here, we just want to simulate neat things happening
echo "sleeping 30s" | tee -a output
sleep 30s

# usually, `output` would have more interesting stuff,
# but we're really only interested in demoing cloudinit completing
echo "setup complete!" | tee -a output

length=$(cat output | wc -c)

# put the output into the output blob
curl \
  -X PUT \
  -T ./output \
  -H "x-ms-version: 2019-02-02" \
  -H "x-ms-date: $(TZ=GMT date '+%a, %d %b %Y %T %Z')" \
  -H "x-ms-blob-type: BlockBlob" \
  -H "Content-Length: $length" \
  -H "Authorization: Bearer $access_token" \
  "${output_blob}"

echo $?
echo "curl finished"
