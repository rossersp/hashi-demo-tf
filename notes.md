# outline

## part zero: why tf?

1. excellent oss support - major cloud providers have teams dedicated to the providers themsevles to address new features, bugs, etc
1. sdk good - recentish updates add a _lot_ of convenience around stuff like retries and more complicated validation usecases (as well as better golang support)
1. mature product - early versions were completely incompatible minor to minor but now is much more robust
1. no dependencies - terraform is a single os architecture specific binary.  if you have an internet connection (or the providers you need mirrored, in, say, artifactory), thats' all you really need!
**(next)**
1. what else is out there? provider native stuff, arm, cf, the respective native CLIs for the providers if you don't have something terribly complicated and don't need to make many if any assertions about state
1. spacelift, which i don't have any personal experience with, but it appears to be a much more pointy-clicky human friendly solution
1. crossplane, which is an interesting approach using custom k8s operators to do the thing in the provider on your behalf.  it's actually spiritually similar to terraform, just using regular ol' k8s yaml instead of a domain-specific language (we'll get to that in a minute)
1. pulumi - another one i've only had a brushing experience with, it cuts the terraform hcl out and provides language-specific wrapper sdks to do things against providers.  It's not terribly mature based on what i've seen but it's got a bit of traction and is nonetheless an interesting idea
**(next)**

## part one: tf plugins: a quick and dirty description of tf plugins

1. all provisioners are plugins
  1. this is what's sourced by tf init
1. written in go, compiles to a single binary
  1. ya can't just run it though
  1. tf is really just a "thin" wrapper around the plugin binaries
    1. tf has two responsibilies: communicating with plugins and making DAGs
  1. tf shells out to the plugins, the plugins start a grpc service, and this is what tf interacts with
    1. doesn't use go plugins, that has kernel requirements in linux, and tf needs to be cross-platform
    1. this becomes important later
1. there are two kinds of provisioners - data and resources
  1. data are for extracting data from stuff that already exists
  1. resources are for making stuff
**(next)**
1. why would you want to roll your own?
  1. internal service
  1. something a vendor doesn't/can't/won't support
  1. the provider doesn't exist yet
  1. fun? i guess? (pizza!)


## part two: let's talk about virtualization

2. at the core, spinning up something new is basically the same
  1. creating state
  1. done creating state
  1. ready state
**(next)**
1. most (all?) cloud providers offer cloud-init functionality
  1. allows for a post-POST script to run to do setup, installs, etc, including azure
**(next)**
1. the demo is written for azure ... but first we need to talk about aws.
  1. on top of the usual events already mentioned, aws adds a bunch more
  1. for our purposes, i'm interested in the cloud-init events
  1. signals for start and end, gives you a rough idea of what's going on at all times ... but... erm...
  **(next)**
1. azure doesn't do that.

## part three: a "real world" problem

1. say for the sake of argument, we want to ape the aws functionality
  1. could be the service takes forever to boot, eg jetty, and we don't want to register it with discovery until it's started
  1. could be that cloudinit does something particularly complicated, like run packer
  1. maybe this is a contrived example
**(next)**
1. how do we do that?
  1. there's a _lot_ of options here.
  1. i chose to publish the install results to blob, waiting for the blob to contain a completed message
    1. has the added benefit of being totally internal to the same tf script
    1. as such can be managed completely from tf
    1. no need for external services
    1. critically, has to work and be repeatable
    1. still kinda gross, i know

## part four: let's take a peek at a provider + how it's used

1. the simple resource provider
  1. makes a schema resource, that's it!
  1. schema
    1. defines CRUD ops
    1. defines what you're expecting in and out
      1. out is denoted by `Computed`
    1. enforces typechecking for you
    1. gives some extra control around behavior
      1. talk about `ForceNew`
  1. take a look at read
    1. again, typechecking and existence is taken care of, so there's no need to check the inputs
      1. if the inputs were optional with no default, we could use `GetOk` instead of `Get`, which returns an additional value for whether or not that value exists
    1. the core of this read is straightforward: grab a blob, check the contents with the passed in regex, if it exists, break, otherwise do until timeout
      1. timeout will return an error, which will cause the tf apply to fail
    1. sidenote: when i started looking at this, i thought this data bag was shared
      1. grpc prevents this kind of sharing, all plugins are self-contained
  1. take a look at delete
    1. unsetting the ID or setting it to empty string will remove the resource from state, provided there's no error to return
    1. here, we're not doing anything, nothing is _really_ created, so the delete is dumb
  1. the provider is registered as a map when a new provider is made (provider.go)
  1. the plugin is served as a provider in main.go
  1. a quick look at the tests
    1. specify what your resource you want to inspect is
    1. let the sdk helper do the heavy lifting
1. ffffinally, some terraform
  1. sourcing the custom provider can be done a couple of ways
    1. in the root directory of where your tf is
    1. in dot-terraform.d (don't do this)
    1. next to the terraform binary
    1. dot-terraform.d in your home directory (or appdata if you're on windows)
    1. in terraform.d (what i did)
  1. tf init
    1. pulls in providers from hashicorp
    1. sources my custom provider from terraform.d
  1. template time!
    1. template one: cloud config - grabs the bootstrap script
    1. template two: bootstrap script - simulates an install and shoves results back into blob storage
  1. and now, the oppression of the main.tf.  the important bits:
    1. `application_install_output` - makes a placeholder where our output will go (this is so tf can manage it at delete and we aren't trying to do anything out of band)
    1. `bootstrap` - our "install" script
    1. user identity + role assignment - makes a user and grants it gawrd access to storage
    1. the vm itself
      1. templates the cloud config with the setup script url
      1. uses the same identity on the vm as in the blob storage
        1. jump back to the application setup - this 169 guff - that's the metadata service that user authenticates against to get the bearer
      1. uses `depends_on` to ensure the setup blob exists as well as the role assignment
        1. without the role assignment, you can get into a nasty race condition, ask me how i know
    1. finally, the good part: our custom provider call
      1. kinda anticlimactic isn't it?
  1. make up
    1. should spit out "success"
    1. hop over to azure ui, take a look at the blob, oooh ahh
  1. make down
1. a more complicated data provider
  1. basic setup is the same - still a schema resource
    1. key difference: only read.  specifying anything else, you'll get runtime problems
  1. let's look at the read
    1. again, same idea, schema checking is done for you
    1. this oppressive wall of if/else is to optionally scan a specific RG, RGs across subscriptions, a specific subscription, or all subscriptions for a resource
    1. the important bit is toward the bottom - it rips through the list of found resources and adds them to the output parameter
  1. and again, some terraform
    1. basically the same
    1. point out the demostorage + random string
    1. jump down to data, show how the ids are extracted, as well as extracting the storage account name
