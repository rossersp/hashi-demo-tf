# things i'd like to do

1. custom vm resource that uses blob location + basic grep to wait for a condition
1. custom data source to find something given a name
1. tf project that uses the blob + grep to wait for vm cloud init to complete
1. tf project that uses blob with a "gitsha", the custom data source to find it, then blob + grep to wait for cloud init to complete
1. cheatsheet for the demo
1. slides

# things that are done

# stuff i wanna talk about

1. `d.Get()` vs `d.GetOk()`
1. the vagaries of `ForceNew`
1. schema typechecking
