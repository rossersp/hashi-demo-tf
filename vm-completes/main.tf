provider "azurerm" {
  version = "1.38.0"
}

resource "random_string" "demo" {
  length  = 10
  special = false
  upper   = false
}

# make a resource group
resource "azurerm_resource_group" "demo" {
  name     = "demo-resource-group4"
  location = "eastus"
}

# we'll need a storage account...
resource "azurerm_storage_account" "demo" {
  name                      = "demostorage${random_string.demo.result}"
  resource_group_name       = azurerm_resource_group.demo.name
  location                  = azurerm_resource_group.demo.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
}

# ... and a container
resource "azurerm_storage_container" "demo" {
  name                  = "democontainer"
  storage_account_name  = azurerm_storage_account.demo.name
  container_access_type = "private"
}

# upload the output resource
# this is so application install can be output and pulled down for debugging
# also required if we want `output` to be managed by tf without fancy imports
resource "azurerm_storage_blob" "application_install_output" {
  storage_account_name   = azurerm_storage_account.demo.name
  storage_container_name = azurerm_storage_container.demo.name
  type                   = "block"
  name                   = "output"
  source_content         = "temporary content to prevent errors"
}

resource "azurerm_storage_blob" "bootstrap" {
  storage_account_name   = azurerm_storage_account.demo.name
  storage_container_name = azurerm_storage_container.demo.name
  type                   = "block"
  name                   = "bootstrap.sh"
  source_content = templatefile("${path.module}/templates/bootstrap.sh", {
    output_blob = azurerm_storage_blob.application_install_output.url
  })
}

# make a demo security group
resource "azurerm_network_security_group" "demo" {
  name                = "demo-security-group"
  location            = azurerm_resource_group.demo.location
  resource_group_name = azurerm_resource_group.demo.name
  security_rule {
    name                       = "demo-nsg-rule"
    priority                   = 1001
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# we'll also need a virtual network...
resource "azurerm_virtual_network" "demo" {
  name                = "demo-vnet"
  address_space       = ["10.0.0.0/27"]
  location            = azurerm_resource_group.demo.location
  resource_group_name = azurerm_resource_group.demo.name
}

# ... and a subnet
resource "azurerm_subnet" "demo" {
  name                 = "demosubnet"
  resource_group_name  = azurerm_resource_group.demo.name
  virtual_network_name = azurerm_virtual_network.demo.name
  address_prefix       = "10.0.0.0/27"
}

# make a network interface for the vm on that subnet
resource "azurerm_network_interface" "demo" {
  name                      = "demo-nic"
  location                  = azurerm_resource_group.demo.location
  resource_group_name       = azurerm_resource_group.demo.name
  network_security_group_id = azurerm_network_security_group.demo.id
  ip_configuration {
    name                          = "demoimagenicconf"
    subnet_id                     = azurerm_subnet.demo.id
    private_ip_address_allocation = "Dynamic"
  }
  depends_on = [azurerm_network_security_group.demo]
}

# make a user assigned identity
resource "azurerm_user_assigned_identity" "demo" {
  name                = "demo-user"
  resource_group_name = azurerm_resource_group.demo.name
  location            = azurerm_resource_group.demo.location
}

# grant that user data contributor to the blob storage
resource "azurerm_role_assignment" "demo" {
  scope                = azurerm_storage_account.demo.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = azurerm_user_assigned_identity.demo.principal_id
}

# make the vm
resource "azurerm_virtual_machine" "demo" {
  name                  = "demo-vm"
  location              = azurerm_resource_group.demo.location
  resource_group_name   = azurerm_resource_group.demo.name
  network_interface_ids = [azurerm_network_interface.demo.id]
  vm_size               = "Standard_DS1_v2"
  storage_os_disk {
    name              = "demovmdisk${formatdate("YYYYMMDDHHmmss", timestamp())}"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  os_profile {
    computer_name  = "demo-vm"
    admin_username = "superadmin"
    admin_password = "Password123!"
    custom_data = base64encode(templatefile("${path.module}/templates/cloudconfig", {
      blob_url = azurerm_storage_blob.bootstrap.url
    }))
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.demo.primary_blob_endpoint
  }
  identity {
    type         = "UserAssigned"
    identity_ids = [azurerm_user_assigned_identity.demo.id]
  }
  depends_on = [
    azurerm_storage_blob.application_install_output,
    azurerm_role_assignment.demo
  ]
}

# finally: wait for that vm to be done with its "install"
resource "azureaux_wait_for_blob_contains" "demo" {
  resource_group_name  = azurerm_resource_group.demo.name
  storage_account_name = azurerm_storage_account.demo.name
  container_name       = azurerm_storage_container.demo.name
  blob_name            = "output"
  regex                = ".*complete.*"
  depends_on           = [azurerm_virtual_machine.demo]
}

# spit out what the state is of the findings!
output "contains_state" {
  value = azureaux_wait_for_blob_contains.demo.state
}
